import { browser, by, element, ElementArrayFinder, ElementFinder } from 'protractor';

export class AppPage {

  navigateTo(): Promise<unknown> {
    console.log(browser.baseUrl);
    return browser.get(browser.baseUrl) as Promise<unknown>;
  }

  getTitleText(): Promise<string> {
    return element(by.css('h1')).getText() as Promise<string>;
  }

  getAllNewsButton(): ElementFinder {
    return element(by.css('.all a[href*=\'Nachrichten\']')) as ElementFinder;
  }

  getUrl(): Promise<string> {
    return browser.getCurrentUrl() as Promise<string>;
  }
}
