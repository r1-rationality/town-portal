# The frontend app of the small town portal

# Development

- Communication with API is managed by src/app/search.service.ts.
- Routing src/app/app-routing.module.ts
- All key UI components are structure in src/app/ folder.
- To build the app, run ng build or ng build --prod for a production build.

# References

This frontend app is using [Angular version 9](https://angular.io/).
