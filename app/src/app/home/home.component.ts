import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SearchService } from '../search.service';
import { ConfigService } from '../config.service';
import { SafeStyle, DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'ta-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public townMotto: string;
  public config: any;
  public categories: any;
  public bgImage: SafeStyle;

  constructor(public searchService: SearchService, private configService: ConfigService,
              private router: Router, private sanitizer: DomSanitizer) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  async ngOnInit() {
    this.config = await this.configService.getConfig();
    this.categories = this.configService.getCategories();
    this.bgImage = this.sanitizer.bypassSecurityTrustStyle('url(/towns/' + this.config.index + '/home.jpeg)');
  }

}
