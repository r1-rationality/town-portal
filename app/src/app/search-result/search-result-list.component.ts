import { Component, OnChanges, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { combineLatest } from 'rxjs';
import { ConfigService } from '../config.service';
import { SearchService } from '../search.service';

class Filter {
  constructor(public name: string, public title: string, public docCount: number, public active: boolean) {}
}

@Component({
  selector: 'ta-search-result-list',
  templateUrl: './search-result-list.component.html',
  styleUrls: ['./search-result-list.component.scss']
})
export class SearchResultListComponent implements OnInit, OnChanges {
  public correction: any;
  public categories: Array<any>;
  public category: string;
  public type: string;
  public query: string;
  public count: number;
  public page = 0;
  public pageSize = 10;
  public pageDocs: Array<any>;
  public dynamicFilters: Array<Filter> = [];
  public filtersEnabled = new Set<string>();
  public filtersTempEnabled = new Set<string>();
  public title: string;
  public error: string;
  public filtersBlockOpen = false;
  private origQueryParams: any;

  constructor(private router: Router, private route: ActivatedRoute, private changeDetector: ChangeDetectorRef,
              private configService: ConfigService, private searchService: SearchService) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  async ngOnInit() {
    this.categories = this.configService.getCategories();
    combineLatest(this.route.params, this.route.queryParams).subscribe(
      async ([ params, qparams ]) => {
        await this.configService.getConfig();
        if ('category' in params) {
          if (!this.configService.hasCategory(params.category)) {
            this.router.navigate(['/']);
            return;
          }
          this.category = params.category;
        }
        this.origQueryParams = qparams;
        this.filtersEnabled = new Set();
        if ('searchParams' in qparams) {
          this.filtersEnabled = new Set(qparams.searchParams.split(','));
        }
        this.filtersTempEnabled = new Set(this.filtersEnabled);
        if ('page' in qparams) {
          let page = parseInt(qparams.page, 10);
          if (page < 0) page = 0;
          this.page = page;
        } else {
          this.page = 0;
        }
        if ('query' in params) this.query = decodeURIComponent(params.query);
        this.pageDocs = undefined;
        this.count = undefined;
        this.error = undefined;
        if (this.category !== undefined) {
          this.ngOnChanges();
        }
      }
    );
  }

  ngOnChanges() {
    this.updatePageDocs();
    this.updateDynamicFilters();
    this.changeDetector.markForCheck();
  }

  updatePageDocs() {
    this.title = this.category;
    this.correction = undefined;
    if (this.query && !this.query.match(/^\s*$/)) this.title += `, Suchergebnisse für "${this.query}"`;
    this.searchService.search(this.query, this.category, this.origQueryParams.searchParams, this.page * this.pageSize, this.pageSize)
      .subscribe(
        resp => {
          if (!this.correction) this.correction = resp._correction;
          this.pageDocs = resp._docs;
          this.count = resp.body.hits.total.value;
          this.title += `: ${this.count} Treffer`;
          this.changeDetector.markForCheck();
        },
        (err) => {
          console.error(err);
          this.error = 'Ihre Suche war nicht erfolgreich';
        }
      );
  }

  _createFilter(bucket: any) {
    return new Filter(bucket.key, bucket.key.split('/')[1], bucket.doc_dount, bucket.doc_count > 0);
  }

  updateDynamicFilters() {
    this.dynamicFilters = [];
    this.type = undefined;
    this.searchService.docCount(this.query, this.category, this.origQueryParams.searchParams).subscribe(
      resp => {
        const aggs = resp.body.aggregations;
        if (aggs.types && aggs.types.buckets.length) this.type = aggs.types.buckets[0].key;
        if (!('categories' in aggs)) {
          console.error(`Unsupported API response`);
          return;
        }
        for (const bucket of aggs.categories.buckets) {
          if (bucket.key.indexOf('/') === -1) continue;
          this.dynamicFilters.push(this._createFilter(bucket));
        }
      },
      err => {
        console.error(err);
      }
    );
  }

  applyDynamicFilters(newFilters: Set<string>) {
    let searchParams = Array.from(newFilters).join(',');
    if (searchParams === ',') searchParams = '';
    const newParams = { ...this.origQueryParams, page: 0 };
    if (searchParams.length) newParams.searchParams = searchParams;
    else if ('searchParams' in newParams) delete newParams.searchParams;
    this.router.navigate([], { queryParams: newParams });
  }

  setDynamicFilter(name, event: MatCheckboxChange) {
    const allParams = new Set(this.filtersEnabled);
    if (event.checked) allParams.add(name);
    else if (allParams.has(name)) allParams.delete(name);
    this.applyDynamicFilters(allParams);
  }

  setPage(page: number) {
    const newParams = { ...this.origQueryParams, page };
    this.router.navigate([], { queryParams: newParams });
  }

  filtersBlockClick() {
    this.filtersBlockOpen = !this.filtersBlockOpen;
    if (this.filtersBlockOpen) this.filtersTempEnabled = new Set(this.filtersEnabled);
  }

  filtersBlockChange(name: string, event: MatCheckboxChange) {
    if (event.checked) this.filtersTempEnabled.add(name);
    else if (this.filtersTempEnabled.has(name)) this.filtersTempEnabled.delete(name);
  }

  filterBlockApply() {
    this.applyDynamicFilters(this.filtersTempEnabled);
  }

  filterBlockClear() {
    this.filtersTempEnabled = new Set();
  }
}
