import { Component, Input } from '@angular/core';

@Component({
  selector: 'ta-search-result-any',
  templateUrl: './any.component.html',
  styleUrls: ['./any.component.scss']
})
export class SearchResultAnyComponent {
  @Input() docs: Array<any>;

  constructor() {
  }

  docsInRows(columns = 2) {
    if (!this.docs) return;
    const rows = [];
    for (const [i, doc] of this.docs.entries()) {
      if (i % columns === 0) rows.push([]);
      rows[rows.length - 1].push(doc);
    }
    while (rows.length > 0 && rows[rows.length - 1].length < columns) {
      rows[rows.length - 1].push(null);
    }
    return rows;
  }
}
