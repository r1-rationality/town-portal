import { Component, Input } from '@angular/core';

@Component({
  selector: 'ta-preview-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class PreviewNewsComponent {
  @Input() doc: any;
}
