import { Component, Input } from '@angular/core';

@Component({
  selector: 'ta-preview-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.scss']
})
export class PreviewCitiesComponent {
  @Input() doc: any;
}
