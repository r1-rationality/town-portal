import { Component, Input } from '@angular/core';

@Component({
  selector: 'ta-preview-any',
  templateUrl: './any.component.html',
  styleUrls: ['./any.component.scss']
})
export class PreviewAnyComponent {
  @Input() doc: any;

  constructor() {
  }
}
