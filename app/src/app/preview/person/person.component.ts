import { Component, Input } from '@angular/core';

@Component({
  selector: 'ta-preview-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.scss']
})
export class PreviewPersonComponent {
  @Input() doc: any;
}
