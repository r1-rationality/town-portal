import { Component, Input } from '@angular/core';

@Component({
  selector: 'ta-preview-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class PreviewEventComponent {
  @Input() doc: any;
}
