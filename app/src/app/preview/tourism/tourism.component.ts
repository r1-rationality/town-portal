import { Component, Input } from '@angular/core';

@Component({
  selector: 'ta-preview-tourism',
  templateUrl: './tourism.component.html',
  styleUrls: ['./tourism.component.scss']
})
export class PreviewTourismComponent {
  @Input() doc: any;
}
