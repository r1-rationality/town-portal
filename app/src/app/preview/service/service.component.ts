import { Component, Input } from '@angular/core';

@Component({
  selector: 'ta-preview-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.scss']
})
export class PreviewServiceComponent {
  @Input() doc: any;
  @Input() linkLabel: string;
  @Input() imageLabel: string;
}
