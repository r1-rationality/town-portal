import { Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { filter } from 'rxjs/operators';
import { MatSidenavContainer, MatSidenavContent } from '@angular/material/sidenav';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { SearchService } from './search.service';
import { ConfigService } from './config.service';
import { of } from 'rxjs';
import { stringify } from 'querystring';

interface MenuTreeNode {
  name: string;
  children: MenuTreeNode[];
}

@Component({
  selector: 'ta-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  @ViewChild(MatSidenavContainer) sidenav: MatSidenavContainer;
  @ViewChild(MatSidenavContent) sidenavContent: MatSidenavContent;
  public config: any;
  public pageUrl: string;
  public menuIcon = 'menu';
  public logoUrl: SafeUrl;

  public treeControl: NestedTreeControl<MenuTreeNode>;
  public dataSource: MatTreeNestedDataSource<MenuTreeNode>;
  private rawCategories: any;
  private menuNumColumns = 1;

  public getChildren = (node: MenuTreeNode) => of(node.children);
  public hasChild = (_: number, node: MenuTreeNode) => node.children.length;

  constructor(private searchService: SearchService, private configService: ConfigService,
              private router: Router, private changeDetector: ChangeDetectorRef,
              private breakpointObserver: BreakpointObserver,
              private sanitizer: DomSanitizer, private route: ActivatedRoute) {
    this.breakpointObserver.observe('(max-width: 576px)').subscribe(result => this.updateMenuColumns(result));
  }

  initTown() {
    const m = window.location.hostname.match(/^(.+?)(.dev|-beta|).r1rationality.com$/);
    if (!m) throw Error('Unsupported domain name format!');
    this.configService.setTown(m[1] + '.de');
  }

  async ngOnInit() {
    this.initTown();
    this.config = await this.configService.getConfig();
    this.rawCategories = this.configService.getCategories();

    this.logoUrl = this.sanitizer.bypassSecurityTrustUrl(`/towns/${this.config.index}/logo.png`);
    this.pageUrl = this.router.url;
    this.router.events.subscribe(event => {
      if (this.sidenav) this.sidenav.close();
      if (this.sidenavContent) this.sidenavContent.scrollTo({ top: 0 });
    });

    this.router.events.pipe(filter(e => e instanceof NavigationEnd)).subscribe((e: NavigationEnd) => this.pageUrl = e.url);
    this.searchService.docCount().subscribe(
      resp => {
        const aggs = resp.body.aggregations;
        if (!('categories' in aggs)) {
          console.error(`Unsupported API response`);
          return;
        }
        const menu = this.rawCategories.reduce(
          (o, c) => ({ ...o, [c.class]: { name: c.class, children: [], link: '/' + c.class, params: {}, level: 0 } }), {}
        );
        for (const bucket of aggs.categories.buckets) {
          if (bucket.key.indexOf('/') === -1) continue;
          const [l1, l2] = bucket.key.split('/', 2);
          if (!(l1 in menu)) continue;
          menu[l1].children.push({ name: l2, children: [], link: '/' + l1, params: { searchParams: bucket.key }, level: 1 });
        }
        const treeMenu = Object.entries(menu).map(m => m[1]) as MenuTreeNode[];
        this.treeControl = new NestedTreeControl<MenuTreeNode>(this.getChildren);
        this.dataSource = new MatTreeNestedDataSource<MenuTreeNode>();
        this.dataSource.data = treeMenu;
      },
      err => {
        console.error(err);
      }
    );
  }

  sidenavOpened() {
    this.menuIcon = 'close';
    this.changeDetector.markForCheck();
  }

  sidenavClosed() {
    this.menuIcon = 'menu';
    this.changeDetector.markForCheck();
  }

  categoriesAll() {
    return [ ...this.rawCategories ];
  }

  categories(cols = 2) {
    const rows = [];
    for (const [i, cat] of this.rawCategories.entries()) {
      if (i % cols === 0) rows.push([]);
      rows[rows.length - 1].push(cat);
    }
    return rows;
  }

  updateMenuColumns(mobile: BreakpointState) {
    this.menuNumColumns = mobile.matches ? 1 : 2;
  }

  removeBorder(s) {
    if (s.target.localName !== 'input') s.target.ownerDocument.activeElement.blur();
  }

  focusLabel() {
    const element: any = document.querySelector('.town-name');
    if (element) {
      element.scrollIntoView({behavior: 'instant', block: 'start'});
      element.focus({preventScroll: false});
    }
  }
}
