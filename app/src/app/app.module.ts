import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FlexLayoutModule } from '@angular/flex-layout';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTreeModule } from '@angular/material/tree';
import { MatToolbarModule } from '@angular/material/toolbar';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CardComponent } from './shared/card/card.component';
import { HomeComponent } from './home/home.component';
import { PageAnyComponent } from './page-any/page-any';
import { PreviewAnyComponent } from './preview/any/any.component';
import { PreviewEventComponent } from './preview/event/event.component';
import { PreviewNewsComponent } from './preview/news/news.component';
import { PreviewPersonComponent } from './preview/person/person.component';
import { PreviewServiceComponent } from './preview/service/service.component';
import { PreviewTourismComponent } from './preview/tourism/tourism.component';
import { PreviewCitiesComponent } from './preview/cities/cities.component';
import { RandomComponent } from './random/random.component';
import { RequestErrorComponent } from './shared/request-error/request-error.component';
import { SearchCorrectionComponent } from './shared/search-correction/search-correction.component';
import { SearchHeaderComponent } from './shared/search-header/search-header.component';
import { SerpHeaderComponent } from './shared/serp-header/serp-header.component';
import { SearchResultAnyComponent } from './search-result/any/any.component';
import { SearchResultListComponent } from './search-result/search-result-list.component';
import { SearchResultCombinedComponent } from './search-result-combined/search-result-combined.component';
import { SearchResultCombinedAnyComponent } from './search-result-combined/any/any.component';

@NgModule({
  declarations: [
    AppComponent,
    CardComponent,
    HomeComponent,
    PageAnyComponent,
    PreviewAnyComponent,
    PreviewEventComponent,
    PreviewNewsComponent,
    PreviewPersonComponent,
    PreviewServiceComponent,
    PreviewTourismComponent,
    PreviewCitiesComponent,
    RandomComponent,
    RequestErrorComponent,
    SearchCorrectionComponent,
    SearchHeaderComponent,
    SearchResultAnyComponent,
    SearchResultListComponent,
    SearchResultCombinedComponent,
    SearchResultCombinedAnyComponent,
    SerpHeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCheckboxModule,
    MatIconModule,
    MatListModule,
    MatPaginatorModule,
    MatSelectModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatTreeModule,
    MatToolbarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
