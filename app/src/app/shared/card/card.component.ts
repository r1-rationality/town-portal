import { Component, Input } from '@angular/core';

@Component({
  selector: 'ta-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent {
  @Input() doc: any;
  @Input() type: string;
  @Input() linkLabel: string;
  @Input() imageLabel: string;
}
