import { Component, Input } from '@angular/core';

@Component({
  selector: 'ta-request-error',
  templateUrl: './request-error.component.html',
  styleUrls: ['./request-error.component.scss']
})
export class RequestErrorComponent {
  @Input() error: string;
}
