import { Component, Input } from '@angular/core';

@Component({
  selector: 'ta-search-correction',
  templateUrl: './search-correction.component.html',
  styleUrls: ['./search-correction.component.scss']
})
export class SearchCorrectionComponent {
  @Input() correction: any;
}
