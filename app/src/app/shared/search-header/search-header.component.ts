import { Component, OnInit, ViewChild, ChangeDetectorRef, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatAutocompleteTrigger } from '@angular/material/autocomplete';
import { combineLatest } from 'rxjs';
import { SearchService } from 'src/app/search.service';
import { ConfigService } from 'src/app/config.service';

export class SearchRequest {
  constructor(public category: string, public query: string) {}
}

class AutocompleOption {
  constructor(public query: string, public docCount: number) {}
}

@Component({
  selector: 'ta-search-header',
  templateUrl: './search-header.component.html',
  styleUrls: ['./search-header.component.scss']
})
export class SearchHeaderComponent implements OnInit {
  @ViewChild(MatAutocompleteTrigger) autocomplete: MatAutocompleteTrigger;
  public query = '';
  public searchParams = '';
  public autocompleteOptions: Array<AutocompleOption>;
  public config: any;
  public indexName: string;
  public townName: string;

  constructor(private router: Router, private searchService: SearchService, private configService: ConfigService,
              private route: ActivatedRoute, private changeDetector: ChangeDetectorRef) {
  }

  async ngOnInit() {
    this.config = await this.configService.getConfig();
    this.indexName = this.configService.getTownId();
    combineLatest(this.route.params, this.route.queryParams).subscribe(
      ([ params, qparams ]) => {
        if ('searchParams' in qparams) this.searchParams = qparams.searchParams;
        else this.searchParams = undefined;
        if ('query' in params) this.query = decodeURIComponent(params.query);
        else this.query = '';
        this.updateAutocompleteOptions();
      }
    );
  }

  onAnchorClick(): void {
    const element: any = document.querySelector('#content');
    if (element) {
      element.scrollIntoView({behavior: 'instant', block: 'start'});
      element.focus({preventScroll: false});
    }
  }

  updateAutocompleteOptions() {
    if (this.query === '') {
      this.autocompleteOptions = [];
      return;
    }
    this.searchService.autocomplete(this.query, 'all').subscribe(
      json => {
        const resp = JSON.parse(json);
        const agg = resp.body.aggregations['unique-terms'];
        this.autocompleteOptions = agg.buckets.map(x => new AutocompleOption(x.key, x.doc_count)).slice(0, 4);
        this.changeDetector.markForCheck();
      },
      err => {
        this.autocompleteOptions = [];
      }
    );
  }

  clearQuery(input) {
    input.value = '';
    this.onQueryChange('');
  }

  onQueryChange(newQuery: string) {
    this.query = newQuery;
    this.updateAutocompleteOptions();
  }

  onSearchClick(emptyQueryIsSpecial = true) {
    let cmd = ['/', 'all', this.query ];
    if (emptyQueryIsSpecial && (!this.query || this.query.match(/^\s*$/))) cmd = [ this.router.url ];
    this.router.navigate(cmd, { queryParams: {} });
  }

  onAutocomplete(newQuery) {
    this.onQueryChange(newQuery);
    this.onSearchClick();
  }

  onKeyDown(event) {
    if (event.key === 'Enter') {
      if (this.autocomplete) this.autocomplete.closePanel();
      this.onSearchClick();
    }
  }

}
