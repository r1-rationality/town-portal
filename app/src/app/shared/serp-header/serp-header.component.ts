import { Component, Input, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/config.service';

interface Breadcrumb {
  name: string;
  link: string;
  params?: object;
}

@Component({
  selector: 'ta-serp-header',
  templateUrl: './serp-header.component.html',
  styleUrls: ['./serp-header.component.scss']
})
export class SerpHeaderComponent implements OnInit {
  @Input() breadcrumbs: Array<Breadcrumb>;
  @Input() title: string;
  @Input() type: string;
  public config: any;

  constructor(private configService: ConfigService) {}

  async ngOnInit() {
    this.config = await this.configService.getConfig();
  }
}
