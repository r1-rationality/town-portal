import { Component, Input, ChangeDetectorRef, OnInit, SecurityContext } from '@angular/core';
import { combineLatest } from 'rxjs';
import { ConfigService } from '../config.service';
import { SearchService } from '../search.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'ta-search-result-combined',
  templateUrl: './search-result-combined.component.html',
  styleUrls: ['./search-result-combined.component.scss']
})
export class SearchResultCombinedComponent implements OnInit {
  @Input() skipDocs: any;
  public categories: Array<any>;
  public correction: any;
  public title: string;
  public docs = {};
  public query = '';
  public counts = {};
  public count: number;
  public error: string;
  public config: any;
  public runningRequests = 0;
  private rawCategories: Array<any>;
  private catScores: any;
  private searchParams: string;

  constructor(private configService: ConfigService, private searchService: SearchService,
              private route: ActivatedRoute, private changeDetector: ChangeDetectorRef,
              private router: Router) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  async ngOnInit() {
    this.config = await this.configService.getConfig();
    this.rawCategories = this.configService.getCategories();
    combineLatest(this.route.params, this.route.queryParams).subscribe(
      ([ params, qparams ]) => {
        if ('query' in params) {
          this.query = decodeURIComponent(params.query);
        } else {
          this.query = '';
        }
        this.updateTitle();
        if ('searchParams' in qparams) this.searchParams = qparams.searchParams;
        else this.searchParams = undefined;
        this.error = undefined;
        this.search();
      }
    );
  }

  updateTitle() {
    if (this.query !== '') this.title = `Suchergebnisse für "${this.query}"`;
    else this.title = 'Alle Seiten';
    if (this.runningRequests > 0) this.title += ': suchen ...';
    else if (this.count !== undefined) this.title += `: ${this.count} Treffer`;
  }

  rankedCategories() {
    const ranked = [ ...this.rawCategories ];
    if (this.query.length) ranked.sort((a, b) => this.catScores[b.class] - this.catScores[a.class]);
    for (const cat of ranked) {
      const basePath = 'url(/assets/' + this.config.index + '/';
      cat.docs = this.docs[cat.class];
      cat.count = this.counts[cat.class];
      cat.link = '/' + encodeURIComponent(cat.class) + '/' + (this.query ? encodeURIComponent(this.query) : '');
    }
    return ranked;
  }

  fetchSome(category: string) {
    this.docs[category] = undefined;
    this.counts[category] = undefined;
    this.runningRequests += 1;
    this.searchService.search(this.query, category, this.searchParams).subscribe(
      resp => {
        if (!this.correction) this.correction = resp._correction;
        this.docs[category] = resp._docs;
        if (this.skipDocs) this.docs[category] = this.docs[category].filter(x => !(x._id in this.skipDocs));
        this.counts[category] = resp.body.hits.total.value;
        this.count += resp.body.hits.total.value;
        if (resp.body.hits.hits.length > 0) this.catScores[category] = resp.body.hits.hits[0]._score;
        this.categories = this.rankedCategories();
        this.runningRequests -= 1;
        this.updateTitle();
        this.changeDetector.markForCheck();
      },
      (error) => {
        this.error = 'Ihre Suche war nicht erfolgreich';
        this.runningRequests -= 1;
        this.updateTitle();
        this.changeDetector.markForCheck();
      }
    );
  }

  search() {
    this.catScores = this.rawCategories.reduce((o: any, x: any) => ({ ...o, [x.class]: 0 }), {});
    this.correction = undefined;
    this.error = undefined;
    this.count = 0;
    for (const c of this.rawCategories) this.fetchSome(c.class);
    this.updateTitle();
  }

}
