import { Component, Input } from '@angular/core';

@Component({
  selector: 'ta-search-result-combined-any',
  templateUrl: './any.component.html',
  styleUrls: ['./any.component.scss']
})
export class SearchResultCombinedAnyComponent {
  @Input() docs: Array<any>;
  @Input() cols = 3;
  @Input() rows = 2;

  docsInRows(numRows = this.rows, numCols = this.cols) {
    if (!this.docs) return;
    const rows = [];
    const maxDocs = numRows * numCols;
    for (const [i, doc] of this.docs.entries()) {
      if (i >= maxDocs) break;
      if (i % numCols === 0) rows.push([]);
      rows[rows.length - 1].push(doc);
    }
    while (rows.length > 0 && rows[rows.length - 1].length < numCols) {
      rows[rows.length - 1].push(null);
    }
    return rows;
  }
}
