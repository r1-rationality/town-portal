import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  private townId: string;
  private error: string;
  private config: any;
  private pending: Promise<any>;

  constructor(private http: HttpClient) {
  }

  _checkConfig(townId = this.townId) {
    if (townId === undefined || !this.config) throw Error(`Unknown townId: ${townId}`);
    if (!this.config.categories) throw Error(`Config is missing required field categories: ${this.config}`);
  }

  setTown(townId: string) {
    if (townId === undefined) throw Error('townId must be defined');
    this.townId = townId;
    this.config = undefined;
    this.error = undefined;
  }

  async getConfig(): Promise<any> {
    if (this.townId === undefined) throw Error('townId must be preinitialized');
    if (this.config !== undefined) return this.config;
    if (this.error !== undefined) throw Error(this.error);
    if (this.pending !== undefined) return await this.pending;
    this.pending = new Promise((resolve, reject) => {
      this.http.get<any>(`/towns/${this.townId}/config.json`).subscribe(
        resp => {
          this.config = { ...resp, index: this.townId };
          this.config.categoriesSet = new Set(this.config.categories);
          resolve(this.config);
        },
        err => {
          this.error = err;
          reject(err);
        }
      );
    });
    return this.pending;
  }

  getTownId() {
    this._checkConfig();
    return this.townId;
  }

  hasCategory(category: string) {
    this._checkConfig();
    return this.config.categoriesSet.has(category);
  }

  getCategories() {
    this._checkConfig();
    return this.config.categories.map(c => ({ class: c, title: c }));
  }

}
