import { Component, Input, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConfigService } from '../config.service';
import { SearchService } from '../search.service';
import { LogService } from '../log.service';
import { MatSelectChange } from '@angular/material/select';

@Component({
  selector: 'ta-page-any',
  templateUrl: './page-any.html',
  styleUrls: ['./page-any.scss']
})
export class PageAnyComponent implements OnInit {
  public doc: any;
  public id: string;
  public breadcrumbs: Array<any>;
  public config: any;
  public moreDocs: Array<any>;
  public ready = false;
  public menu: Array<any> = [];
  public menu2: Array<any> = [];
  public altCategories: Array<string> = [];

  constructor(private route: ActivatedRoute, private configService: ConfigService, private logService: LogService,
              private searchService: SearchService, private changeDetector: ChangeDetectorRef) {
  }

  async ngOnInit() {
    this.config = await this.configService.getConfig();
    this.route.params.subscribe(
      params => {
        if (!('id' in params)) throw Error('required id parameter is missing!');
        this.id = params.id;
        this.searchService.getDoc(this.id).subscribe(
          (resp: any) => {
            if (resp.body.hits.hits.length) {
              const hit = resp.body.hits.hits[0];
              this.searchService.moreLikeThat(hit._id, 3).subscribe(
                respMore => {
                  this.moreDocs = respMore.body.hits.hits.map(x => ({ ...x._source, _id: x._id }));
                }
              );
              this.doc = hit._source;
              this.breadcrumbs = [];
              const [ docL1, docL2 ] = this.doc.category.split('/', 2);
              this.breadcrumbs.push({ name: docL1, link: '/' + docL1 });
              if (docL2) this.breadcrumbs.push({ name: docL2, link: '/' + docL1, params: { searchParams: this.doc.category } });
              this.searchService.docCount('', this.doc.categoryL1).subscribe(
                respCnt => {
                  const aggs = respCnt.body.aggregations;
                  this.menu = [];
                  for (const bucket of aggs.categories.buckets) {
                    if (bucket.key.indexOf('/') === -1) continue;
                    const [l1, l2] = bucket.key.split('/', 2);
                    const active = l1 === docL1 && l2 === docL2;
                    this.menu.push({ name: l2, link: '/' + l1, params: { searchParams: bucket.key }, active });
                  }
                  this.menu2 = this.configService.getCategories()
                    .map(c => ({ name: c.title, link: '/' + c.class, active: c.class === docL1 }));
                },
                err => console.error(err)
              );
              this.searchService.docCount().subscribe(
                respCnt => {
                  this.altCategories = respCnt.body.aggregations.categories.buckets.map(b => b.key);
                },
                err => console.error(err)
              );
            }
            this.ready = true;
            this.changeDetector.markForCheck();
          },
          (err: any) => {
            this.ready = true;
            this.doc = undefined;
            this.changeDetector.markForCheck();
          }
        );
      }
    );
  }

  newAltCategory(event: MatSelectChange) {
    this.logService.log('page-category', `${JSON.stringify(this.doc.url)},${JSON.stringify(event.value)}`).subscribe(
      resp => {
      },
      err => {
        console.error(err);
      }
    );
  }

}
