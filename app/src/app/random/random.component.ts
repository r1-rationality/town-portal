import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { SearchService } from '../search.service';

@Component({
  selector: 'ta-random',
  templateUrl: './random.component.html',
  styleUrls: ['./random.component.scss']
})
export class RandomComponent implements OnInit {
  public docs: Array<any>;

  constructor(private searchService: SearchService, private changeDetector: ChangeDetectorRef) {}

  ngOnInit() {
    console.log(this.constructor.name, 'ngOnInit');
    this.searchService.randomDocs().subscribe(
      resp => {
        console.log(resp);
        this.docs = resp.body.hits.hits.map(x => ({ ...x._source, _id: x._id }));
        this.changeDetector.markForCheck();
      }
    );
  }

}
