import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PageAnyComponent } from './page-any/page-any';
import { RandomComponent } from './random/random.component';
import { SearchResultListComponent } from './search-result/search-result-list.component';
import { SearchResultCombinedComponent } from './search-result-combined/search-result-combined.component';

const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'all/:query', component: SearchResultCombinedComponent, pathMatch: 'full' },
  { path: 'random', component: RandomComponent, pathMatch: 'full' },
  { path: 'page/:id', component: PageAnyComponent },
  { path: ':category', component: SearchResultListComponent },
  { path: ':category/:query', component: SearchResultListComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      {
        scrollPositionRestoration: 'enabled',
        anchorScrolling: 'enabled',
        onSameUrlNavigation: 'reload'
      }
    )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
