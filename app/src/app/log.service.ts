import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LogService {

  constructor(private http: HttpClient) {}

  log(name: string, message: string): Observable<string> {
    const url = '/api/log/' + encodeURIComponent(name);
    const httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Cache-Control', 'no-cache');
    return this.http.post(url, JSON.stringify(message), {responseType: 'text', headers: httpHeaders});
  }

}
