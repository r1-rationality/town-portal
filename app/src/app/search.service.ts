import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient, private configService: ConfigService) {}

  autocomplete(query: string, category: string = 'all'): Observable<string> {
    if (!query || query === '') query = ' ';
    const params = new HttpParams().append('query', query);
    const url = '/api/autocomplete/' + encodeURIComponent(this.configService.getTownId()) + '/' + encodeURIComponent(category);
    return this.http.get(url, { responseType: 'text', params });
  }

  docCount(query = '', category = 'all', queryParams?: string, term?: string) {
    if (!query || query === '') query = ' ';
    let params = new HttpParams().append('query', query);
    if (queryParams !== undefined) params = params.append('searchParams', queryParams);
    if (term !== undefined) params = params.append('term', term);
    const url = '/api/doc-count/' + encodeURIComponent(this.configService.getTownId()) + '/' + encodeURIComponent(category);
    return this.http.get<any>(url, { params });
  }

  search(query = '', category = 'all', queryParams?: string, offset: number = 0, size: number = 10): Observable<any> {
    if (!query || query === '') query = ' ';
    let params = new HttpParams().append('query', query);
    if (queryParams) params = params.append('searchParams', queryParams);
    if (offset) params = params.append('offset', offset.toString());
    if (size && size !== 10) params = params.append('size', size.toString());
    const url = '/api/search/' + encodeURIComponent(this.configService.getTownId()) + '/' + encodeURIComponent(category);
    return this.http.get(url, { params }).pipe(map((resp: any) => {
      if (resp.body.suggest.simple.length && resp.body.suggest.simple[0].options.length) {
        resp._correction = resp.body.suggest.simple[0].options[0];
      }
      resp._docs = resp.body.hits.hits.map(x => ({
        ...x._source, _id: x._id,
        _categoryHighlight: x.highlight && x.highlight.categoryText ? x.highlight.categoryText.join(' ... ') : undefined,
        _contentHighlight: x.highlight && x.highlight.content ? '... ' + x.highlight.content.join(' ... ') + ' ...' : undefined,
        _titleHighlight: x.highlight  && x.highlight.title ? x.highlight.title.join(' ... ') : undefined,
      }));
      return resp;
    }));
  }

  getDoc(id: string): Observable<string> {
    return this.http.get<any>('/api/doc/' + encodeURIComponent(this.configService.getTownId()) + '/' + encodeURIComponent(id));
  }

  moreLikeThat(id: string, size: number = 10): Observable<any> {
    let params = new HttpParams();
    if (size && size !== 10) params = params.append('size', size.toString());
    return this.http.get('/api/more/' + encodeURIComponent(this.configService.getTownId()) + '/' + encodeURIComponent(id), { params });
  }

  randomDocs(size: number = 10): Observable<any> {
    let params = new HttpParams();
    if (size && size !== 10) params = params.append('size', size.toString());
    return this.http.get<any>('/api/random/' + encodeURIComponent(this.configService.getTownId()));
  }
}
