const apify = require('apify');
const { Client } = require('@elastic/elasticsearch');
const minimistParseArgv = require('minimist');
const promisify = require('util').promisify;
const fs = require('fs');
const imageSize = require('image-size');
fs.existsAsync = promisify(fs.exists);
fs.readFileAsync = promisify(fs.readFile);
fs.renameAsync = promisify(fs.rename);

class Importer {

  constructor(client, updates) {
    this.client = client;
    this.updates = updates;
    this.imagesPathPrefix = '.';
  }

  async createIndex() {
    this.indexName = this.updates.index + '-' + Math.random().toString(36).substring(2, 15);
    console.log("Creating index", this.indexName);
    await this.client.indices.create({ index: this.indexName, body: this.updates.schema });
  }

  async delIndexIfExists(index) {
    const exists = await this.client.indices.exists({ index });
    console.log('existsIndex', index, exists.body);
    if (exists.body) await this.client.indices.delete({ index });
  }

  async updateAlias() {
    const existsAlias = await this.client.indices.existsAlias({ name: this.updates.index });
    if (existsAlias.body) {
      const alias = await this.client.indices.getAlias({ name: this.updates.index });
      for (let index of Object.keys(alias.body)) {
        console.log('Deleting index', index);
        await this.client.indices.delete({ index });
      }
    }
    // Temporary support for directly created indices 
    await this.delIndexIfExists(this.updates.index);
    console.log(`Setting alias ${this.updates.index} pointing to ${this.indexName}`);
    await this.client.indices.putAlias({ index: this.indexName, name: this.updates.index });
  }

  async importDocs() {
    console.log("Importing", this.updates.docs.length, "documents into index", this.indexName);
    const batchSize = 100;
    for (let bi = 0; bi < this.updates.docs.length; bi += batchSize) {
      const docs = this.updates.docs.slice(bi, bi + batchSize).map(doc => {
        doc.categoryText = doc.category;
        doc.suggest = doc.category + " " + doc.title + " " + doc.content;
        doc.categoryL1 = doc.category.split("/")[0];
        return doc;
      });
      const body = docs.flatMap(doc => [ { index: { _index: this.indexName } }, doc ]);
      const resp = await this.client.bulk({ refresh: true, body });
      if (!resp.body.errors) continue;
      console.error(`Indexing errors for block starting at doc #${bi}:`);
      resp.body.items.forEach((action, i) => {
        const operation = Object.keys(action)[0];
        if (!action[operation].error) return;
        console.error({
          index: bi + i,
          status: action[operation].status,
          error: action[operation].error,
          operation: body[i * 2],
          document: body[i * 2 + 1]
        });
      });
    }
  }

  async crawlImages() {
    const pathPrefix = this.imagesPathPrefix;
    console.log("Downloading", Object.keys(this.updates.images).length, "images");
    const sources = [];
    const images = this.updates.images;
    for (let [url, path] of Object.entries(images)) {
      if (await fs.existsAsync(pathPrefix + path)) continue;
      sources.push({ url });
    }
    const requestList = new apify.RequestList({ sources });
    await requestList.initialize();
    
    const crawler = new apify.BasicCrawler({
      requestList,
      handleRequestFunction: async ({ request }) => {
        const reqOpts = { url: request.url, stream: true };
        const response = await apify.utils.requestAsBrowser(reqOpts);
        if (response.statusCode === 200) {
          await new Promise((resolve, reject) => {
            const tmpPath = pathPrefix + images[request.url] + ".tmp";
            const finalPath = pathPrefix + images[request.url];
            const file = fs.createWriteStream(tmpPath);
            let size = 0;
            response
              .pipe(file)
              .on("finish", async () => file.close());
            response.on("data", (chunk) => size += chunk.length);
            response.on("error", (error) => {
              console.log("ERROR", imagePath[request.url], error);
              reject(error);
            });
            file.on("close", async () => {
              await fs.renameAsync(tmpPath, finalPath);
              console.log(request.url, images[request.url], response.statusCode, size);
              resolve(size);
            });
          });
        } else {
          console.log("ERROR status code != 200:", response.statusCode);
        }
      },
    });
    
    await crawler.run();
  }

  async processImages() {
    const imageWidth = {};
    for (let [url, path] of Object.entries(this.updates.images)) {
      const imagePath = this.imagesPathPrefix + path;
      try {
        if (!await fs.existsAsync(imagePath)) continue;
        const dim = imageSize(imagePath);
        imageWidth[path] = dim.width;
      } catch (err) {
      }
    }
    for (let doc of this.updates.docs) {
      if (doc.image && doc.image in imageWidth) {
        if (imageWidth[doc.image] < 64) {
          delete doc.image;
        }
      }
    }
  }

};

(async () => {
  const args = minimistParseArgv(process.argv.slice(2), { boolean: [ 'images' ], default: { images: true } });
  if (args._.length < 1) {
    console.log("Usage node import.js [--no-images] <example/data.json>");
    return 1;
  }
  if (!process.env.APIFY_MEMORY_MBYTES) process.env.APIFY_MEMORY_MBYTES = 16;
  const path = args._[0];
  const updates = JSON.parse(await fs.readFileAsync(path));
  const client = new Client({ node: 'http://localhost:9200' });
  const importer = new Importer(client, updates);
  if (args.images) await importer.crawlImages();
  await importer.processImages();
  try {
    await importer.createIndex();
    await importer.importDocs();
    await importer.updateAlias();
  } catch (err) {
    console.log(err, err.stack);
    if (this.indexName) {
      console.log(`Deleteing a half-made index ${this.indexName}`);
      await this.delIndexIfExists(this.indexName);
    }
  }
  
})();
