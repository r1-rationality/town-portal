const assert=require('assert')

describe('check search ', () => {                    
    xit('check short query', function () {   
        browser.url('https://toenisvorst-beta.r1rationality.com/');
        const elem = $('input');
        const searchButton = $('#search');
        elem.setValue('Nachrichten');
        searchButton.click()
        browser.pause(4000);
        const result = $('h1.h3').getText().match(/\d{1,}/);
        let num = Number(result);
        expect(num).not.toBe(0);
        expect(num).toBeGreaterThan(1); 
        console.log("num: " + num);  
    })
    
    xit('check long query', function () {    
        browser.url('https://toenisvorst-beta.r1rationality.com/');
        const elem = $('input');
        const searchButton = $('#search');
        elem.setValue('Wie bekomme ich einen Reisepass?');
        searchButton.click()
        browser.pause(4000);
        const result = $('h1.h3').getText().match(/\d{1,}/);
        let num = Number(result);
        expect(num).not.toBe(0);
        expect(num).toBeGreaterThan(1); 
        console.log("num: " + num);
    })
    
    xit('check rare request', function () {    
        browser.url('https://toenisvorst-beta.r1rationality.com/');
        const elem = $('input');
        const searchButton = $('#search');
        elem.setValue('Nersdunck ahn der Heggen');
        searchButton.click()
        browser.pause(4000);
        const result = $('h1.h3').getText().match(/\d{1,}/);
        let num = Number(result);
        expect(num).not.toBe(0);
        console.log("num: " + num);
    })

    xit('check query with misprint', function () {    
        browser.url('https://toenisvorst-beta.r1rationality.com/');
        const elem = $('input');
        const searchButton = $('#search')
        elem.setValue('Nachrchten');
        browser.pause(2000);
        searchButton.click()
        //browser.pause(4000);
        expect($(".correction").getText()).toEqual("Ist es möglich, dass Sie nach \"nachrichten\" suchen?");

    })

    xit('check query with date', function () { 
        browser.url('https://toenisvorst-beta.r1rationality.com/');
        const elem = $('input');
        const searchButton = $('#search')
        elem.setValue('20.04.1928');
        searchButton.click()
        browser.pause(2000);
        const result = $('h1.h3').getText().match(/\d{1,}/);
        let num = Number(result);
        expect(num).not.toBe(0);
        console.log("num: " + num);
    })  

    xit('check min characters in the query', function () { 
        browser.url('https://toenisvorst-beta.r1rationality.com/');
        const elem = $('input');
        const searchButton = $('#search')
        elem.setValue('Öl');
        searchButton.click()
        browser.pause(2000);
        const result = $('h1.h3').getText().match(/\d{1,}/);
        let num = Number(result);
        expect(num).not.toBe(0);
        console.log("num: " + num);
    })
    
    xit('check max characters in the query', function () { 
        browser.url('https://toenisvorst-beta.r1rationality.com/');
        const elem = $('input');
        const searchButton = $('#search')
        elem.setValue('Der Ausschuss hat dem Bewässerungskonzept für städtische Bäume zugestimmt.Dazu gehört auch eine freiwillige Bewässerungsmaßnahme durch die Bevölkerung in Form von Bewässerungssäcken. Die können von Montag, 22. Juni, bis einschließlich Mittwoch, 24. Juni, beim Team für Umwelt und Planung unter 02151/999-406 oder aber unter carsten.kuhnen@toenisvorst.de vorbestellt werden. Im Anschluss daran werden die Bewässerungssäcke nach einer Terminvergabe und Einhaltung der Coronaschutz-Maßnahmen kostenlos ausgegeben. Wie die Säcke funktionieren? Gedacht sind sie für Jungbäume mit einem Stammdurchmesser bis 8 Zentimeter. Im Einzelfall kann man auch zwei Säcke zusammenfügen für einen Stammdurchmesser bis 20 Zentimeter. Ziel ist es, durch die kontinuierliche Wasserabgabe über die Säcke eine Tiefenbewässerung zu ermöglichen. Das führt auch dazu, dass die Wurzeln der Jungbäume – wie vorgesehen - in die Tiefe wachsen können und nicht flachwurzelnd an der Oberfläche bleiben. Keinen Sinn machen die Bewässerungssäcke bei größeren Bäume, da diese nicht mehr so viele Feinwurzeln in Stammnähe haben. Bewässerungssäcke hat die Stadt nach dem Beschluss jetzt gekauft. Die kostenlose Abgabe der Bewässerungssäcke erfolgt gegen Angabe von Name, Straße, Hausnummer, Telefonnummer inklusive Baumnummer. Die Bewässerungen werden auch von der Verwaltung kontrolliert. Aktuell besitzt die Stadt rund 5200 Bäume, darunter rund 500 Jungbäume.');
        searchButton.click()
        browser.pause(2000);
        const result = $('h1.h3').getText().match(/\d{1,}/);
        let num = Number(result);
        expect(num).not.toBe(0);
        console.log("num: " + num);
    })

    xit('check nonexistent request', function () { 
        browser.url('https://toenisvorst-beta.r1rationality.com/');
        const elem = $('input');
        const searchButton = $('#search')
        elem.setValue('Haffmans');
        searchButton.click()
        browser.pause(2000);
        expect($(".container h2").getText()).toEqual("Es tut uns leid, aber es gibt kein Ergebniss.");
    })

    xit('check incorrect request', function () { 
        browser.url('https://toenisvorst-beta.r1rationality.com/');
        const elem = $('input');
        const searchButton = $('#search')
        elem.setValue('#$%^&*()_)');
        searchButton.click()
        browser.pause(2000);
        expect($(".container h2").getText()).toEqual("Es tut uns leid, aber es gibt kein Ergebniss.");
    })

})

xdescribe('check popular bugs', () => { 
    it('search broken emails', function () {    
        browser.url('https://bedburg-hau-beta.r1rationality.com/');
        const elem = $('input');
        const blogButton = $('#search')
        elem.setValue('(at)');
        browser.pause(2000);
        blogButton.click()
        browser.pause(4000);
        expect($(".container h2").getText()).toEqual("Es tut uns leid, aber es gibt kein Ergebniss.");
    })
})

xdescribe('check clickability', () => {
    it('should detect if namburgerButton is clickable', () => {  //check namburgerButton is clickable
        browser.url('https://schelklingen-beta.r1rationality.com/');
        const namburgerButton = $('[aria-label="Menü"]')
        clickable = namburgerButton.isClickable();
        console.log("IS CLICKABLE?: "+ clickable); // outputs: true or false
    
        // wait for element to be clickable
        //browser.waitUntil(() => namburgerButton.isClickable())
    });

    xit('should detect if titleButton is clickable', () => {  //!!!!!!!!!!check main title is clickable
        browser.url('https://schelklingen-beta.r1rationality.com/');
        const titleButton = $('h1.h2')
        clickable = titleButton.isClickable();
        console.log("IS CLICKABLE?: "+ clickable); // outputs: true or false
        //console.log(titleButton.getText())
    });

    it('should detect if townnameButton is clickable', () => {  //check town name is clickable
        browser.url('https://schelklingen-beta.r1rationality.com/');
        const townnameButton = $('.town-name')
        clickable = townnameButton.isClickable();
        console.log("IS CLICKABLE?: "+ clickable); // outputs: true or false
    });
    
    it('should detect if hambcategoryButton is clickable', () => {  //check category in is clickable
        browser.url('https://schelklingen-beta.r1rationality.com/');
        const hambmenuButton = $('[aria-label="Menü"]')
        const hambcategoryButton = $('[href="/Nachrichten"]')
        hambmenuButton.click()
        clickable = hambcategoryButton.isClickable();
        console.log("IS CLICKABLE?: "+ clickable); // outputs: true or false
    });
})

xdescribe('check an element is visible', () => {
    it('should detect if an element is displayed', () => {  //check the element is visible 
        browser.url('https://schelklingen-beta.r1rationality.com/');
        const hambmenuButton = $('[class="non-first"] a[href*="r1rationality"]')
        isDisplayed = hambmenuButton.isDisplayed();
        console.log("IS DISPLAYED?: "+ isDisplayed); // outputs: true
    });

    it('should detect if an element is visible', () => {      //check the element is visible without scrolling
        browser.url('https://schelklingen-beta.r1rationality.com/');
        let isDisplayedInViewport = $('[class="non-first"] a[href*="r1rationality"]').isDisplayedInViewport();
        console.log("isDisplayedInViewport:" + isDisplayedInViewport); // outputs: false
    });
})

xdescribe('accessability', () => {
    before(() => {
        browser.enablePerformanceAudits()
    })
 
    it('should load within performance budget', () => {
 
        browser.url('http://json.org')
 
        let metrics = browser.getMetrics()
        assert.ok(metrics.speedIndex < 1500) // check that speedIndex is below 1.5ms
 
        let score = browser.getPerformanceScore() // get Lighthouse Performance score
        assert.ok(score >= .99) // Lighthouse Performance score is at 99% or higher
 
        $('=Esperanto').click()
 
        metrics = browser.getMetrics()
        assert.ok(metrics.speedIndex < 1500)
        score = browser.getPerformanceScore()
        assert.ok(score >= .99)
    })
 
    after(() => {
        browser.disablePerformanceAudits()
    })
})
