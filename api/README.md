# A Backend API
The goals of this API service are to support the frontend with specialized data-processing tasks and to isolate access to the database.

# How to modify
The file that is responsible for the API description is src/controllers/search.controller.ts
To build the API, simply run npm run build.

# References
This API uses
[![LoopBack](https://github.com/strongloop/loopback-next/raw/master/docs/site/imgs/branding/Powered-by-LoopBack-Badge-(blue)-@2x.png)](http://loopback.io/)
