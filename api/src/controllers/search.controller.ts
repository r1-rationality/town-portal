import fs from 'fs';
import { promisify } from 'util';
import { get, post, param, requestBody } from '@loopback/rest';
import { Client } from '@elastic/elasticsearch';

const EscapeRegexChars = (text: string) => {
  return text.replace(/[-[\]{}()*+?.,\\^$|#\\s]/g, '\\$&');
};

const Filters = new Map<string, any>([
  [ "last7days", { range: { "date": { "gte": "now-7d/d", "lte": "now" } } } ],
  [ "last30days", { range: { "date": { "gte": "now-30d/d", "lte": "now" } } } ],
  [ "last365days", { range: { "date": { "gte": "now-365d/d", "lte": "now" } } } ],
  [ "next7days", { range: { "date": { "lte": "now/d+7d/d", "gte": "now" } } }  ],
  [ "next30days", { range: { "date": { "lte": "now/d+30d/d", "gte": "now" } } } ],
  [ "next365days", { range: { "date": { "lte": "now/d+365d/d", "gte": "now" } } } ]
]);

export class SearchController {
  public client: Client;

  constructor() {    
    this.client = new Client({ node: 'http://search:9200' });
  }

  @post('/log/{name}', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'string'
            }
          }
        }
      }
    }
  })
  async log(
      @param.path.string('name') name: string,
      @requestBody() message: string
      ): Promise<string> {
    if (!name ||name.length > 64) throw Error('log name must have 64 characters max!');
    const safeName = name.replace(/\W/, '-').replace(/-+/, '-');
    const writeFileAsync = promisify(fs.writeFile);
    const logMessage = new Date().toISOString() + ' ' + message + '\n';
    await writeFileAsync(`/log/${safeName}.log`, logMessage, { flag: 'a+', encoding: 'utf8' });
    return 'OK';
  }

  @get('/doc/{index}/{id}')
  async document(
      @param.path.string('index') index: string,
      @param.path.string('id') id: string
      ): Promise<string> {
    let result;
    try {
      result = await this.client.search({
        index,
        size: 1,
        body: {
          "query": {
            "term": {
              "urlNew": "/page/" + id
            }
          }
        }
      });
    } catch (err) {
      console.error(`search ERROR: ${err}`);
      throw(err);
    }
    
    return JSON.stringify(result, null, 4);
  }

  _makeESQuery(category: string, query: string, params: string) {
    const searchParams = params ? new Set(params.split(',')) : undefined;
    if (query === undefined || query === " ") query = "";
    let must = [];
    let should = [];
    if (query !== "") must.push({ multi_match: { query, fields: [ "content", "title^2", "categoryText^1.5" ], "type": "cross_fields", "minimum_should_match": "90%" } });
    if (category !== 'all') must.push({ term: { "categoryL1": category } });
    if (searchParams) {
      for (let param of searchParams) {
        should.push({ term: { "category": param } })
      }
    }
    let esQuery: any;
    if (must.length + should.length > 0) {
      esQuery = { bool: {} };
      if (must.length) esQuery.bool.must = must;
      if (should.length) {
        esQuery.bool.should = should;
        esQuery.bool.minimum_should_match = 1;
      }
    }
    else esQuery = { match_all: {} };
    return esQuery;
  }

  @get('/search/{index}/{category}')
  async search(
      @param.path.string('index') index: string,
      @param.path.string('category') category: string,
      @param.query.string('query') query: string,
      @param.query.string('searchParams') params: string,
      @param.query.string('size') size: number,
      @param.query.string('offset') offset: number
    ): Promise<string>
  {
    let result: any, req: any;
    try {
      console.log('search with params:', category, query, query.match(/^\s*$/) && category === "news", params);
      const esQuery = this._makeESQuery(category, query, params);
      req = { query: esQuery };
      if (!size) size = 10;
      if (!offset) offset = 0;
      if (query.match(/^\s*$/) && category === "Nachrichten")
        req.sort = [ { "typeParams.date": { "order": "desc", "nested": { "path": "typeParams" } } }, { "created": { "order": "desc" } } ];
      if (query.match(/^\s*$/) && category === "Veranstaltungen")
        req.sort = [ { "typeParams.date": { "order": "asc", "nested": { "path": "typeParams" } } }, { "created": { "order": "asc" } } ];
      req.suggest = {
        "text": query,
        "simple": {
          "phrase": {
            "field": "suggest.trigram",
            "size": 1,
            "gram_size": 3,
            "direct_generator": [ {
              "field": "suggest.trigram",
              "suggest_mode": "popular"
            } ],
            "collate": {
              "query": {
                "source": {
                  "multi_match": {
                    "query": "{{suggestion}}",
                    "fields": [ "content", "title^2", "categoryText^1.5" ],
                    "type": "cross_fields",
                    "minimum_should_match": "90%"
                  }
                }
              }
            },
            "highlight": {
              "pre_tag": "<em>",
              "post_tag": "</em>"
            }
          }
        }
      };
      req.highlight = {
        "fields": {
          "content": {},
          "title": {},
          "categoryText": {}
        }
      };
      result = await this.client.search({
        index,
        size,
        from: offset,
        body: req
      });
    } catch (err) {
      console.error(`search ERROR: ${err}, ${JSON.stringify(req, null, 4)}`);
      throw(err);
    }
    
    return JSON.stringify(result, null, 4);
  }

  @get("/random/{index}")
  async random(
    @param.path.string('index') index: string,
    @param.query.string('size') size: number
    ): Promise<string>
  {
    if (!size) size = 10;
    const result = await this.client.search({
      index,
      size,
      body: {
        "query": {
          "function_score": {
            "functions": [
                {
                "random_score": {
                  "seed": Date.now()
                }
              }
            ]
          }
        }
      }
    });

    return JSON.stringify(result, null, 4);
  }

  @get("/more/{index}/{id}")
  async more(
    @param.path.string('index') index: string,
    @param.path.string('id') id: string,
    @param.query.string('size') size: number
    ): Promise<string>
  {
    if (!size) size = 10;
    const result = await this.client.search({
      index,
      size,
      body: {
        "query": {
          "more_like_this": {
            "fields": [ "title^2", "content", "categoryText^1.5" ],
            "like": [ { "_id": id } ]
          }
        }
      }
    });
    return JSON.stringify(result, null, 4);
  }

  @get('/doc-count/{index}/{category}')
  async docCount(
      @param.path.string('index') index: string,
      @param.path.string('category') category: string,
      @param.query.string('term') term: string,
      @param.query.string('query') query: string,
      @param.query.string('searchParams') params: string
    ): Promise<string>
  {
    let result;
    try {
      console.log('search with params:', params);
      const esQuery = this._makeESQuery(category, query, "");
      let aggs = {
        "categories": { "terms": { "field": "category", "size": 300, "order": { "_key": "asc" } } },
        "categoriesL1": { "terms": { "field": "categoryL1", "size": 300, "order": { "_key": "asc" } } },
        "types": { "terms": { "field": "type", "size": 10,  "order": { "_count": "desc" } } }
      };
      result = await this.client.search({
        index, 
        body: {
          "query": esQuery,
          "aggs": aggs
        },
        size: 0
      });
    } catch (err) {
      console.error(`search ERROR: ${err}`);
      throw(err);
    }
    return JSON.stringify(result, null, 4);
  }
  

  // Todo: add search params once they are working well
  @get('/autocomplete/{index}/{category}')
  async autocomplete(
      @param.path.string('index') index: string,
      @param.path.string('category') category: string,
      @param.query.string('query') query: string
    ): Promise<string>
  {
    let result;
    try {
      if (query === undefined || query === " ") query = "";
      let must = [];
      if (query !== "") must.push({ match_bool_prefix: { suggest: { query, analyzer: "german_plain_suggest" } } });
      //if (query !== "") must.push({ multi_match: { type: "bool_prefix", query, fields: [ "suggest", "suggest._2gram", "suggest._3gram" ] } });
      if (category !== 'all') must.push({ term: { "category": category } });
      let esQuery;
      if (must.length > 0) esQuery = { bool: { must } };
      else esQuery = { match_all: {} };
      //console.log("API: esQuery:", JSON.stringify(esQuery, null, 4));
      result = await this.client.search({
        index, size: 0,
        body: {
          "query": esQuery,
          "aggs": {
            "unique-terms": {
              "terms": {
                "field": "suggest",
                "include": ".*" + EscapeRegexChars(query.toLowerCase()) + ".*"
              }
            }
          }
        }
      });
    } catch (err) {
      console.error(`search ERROR: ${err}`);
      throw(err);
    }
    
    return JSON.stringify(result, null, 4);
  }

}
