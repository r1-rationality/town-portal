# Open Source Town Portal
A modern town portal focused on the ease of information discovery:
- It takes any original content that exists.
- It enriches it with modern presentation and information-discovery techniques.
- It works with any existing Content Management System.

## Key features
- For citizens:
  - A navigation system supporting three alternative patterns of navigation:
    - Users who precisely know what they need: search.
    - Users who roughly know what they need: menu and section catalog.
    - Users who don't have a precise objective: sections lists, preview blocks, similar documents, and relevant links patterns.
  - A modern fully-functioning document search with a suggester, typo corrector, and intelligent snippets.
  - Preview blocks for the popular content types: services, events, news, employee directory, touristic attractions, etc.
  - A fully-responsive presentation supporting all device types: mobile phones, tablets, and computers.
  - Full support for Web Content Accessibility Guidelines, reaching 100% Google accessibility score with default settings.
  - Built-in HTTPS support.
- For town employees:
  - Simple to setup and can reduce the time needed to refresh your town's website to just weeks from months-to-years with the traditional approach.
  - Compatible with any content management system and requires no training of employees responsible for content publication.
  - Coming soon: Progressive Web App features, such as push notifications to keep citizens informed.

# Examples

## Overview video
[![A video overview of key features](https://img.youtube.com/vi/EXlbVCVY1pA/0.jpg)](https://youtu.be/EXlbVCVY1pA)

## Links

|Town          |Country|Transformed                                                  |                                Original|
|--------------|-------|-------------------------------------------------------------|----------------------------------------|
|Holzwickede   |Germany|[transformed](https://holzwickede.r1rationality.com/)        |[original](https://holzwickede.de/)     |
|Wachtendonk   |Germany|[transformed](https://wachtendonk.r1rationality.com/)        |[original](https://wachtendonk.de/)     |
|Lauf          |Germany|[transformed](https://lauf.r1rationality.com/)               |[original](https://lauf.de/)            |

# Quick start

## 0. Download and install the required software
- [Docker](https://www.docker.com/get-started).
- [Node.js 12](https://nodejs.org/en/download/).

## 1. Clone the code repository
- git clone --depth=1 https://gitlab.com/r1-rationality/town-portal.git 
- cd town-portal

## 2. Run shell commands to launch local services
- docker-compose build
- docker-compose up -d
- give the search component up to 30 seconds to initialize.
  - You can follow the process by running docker-compose logs -f search

## 3. Run shell commands to import sample town data
- npm i
- node import.js example/data.json
- mkdir -p app/src/towns/town.de
- cp example/assets/* app/src/towns/town.de/

## 4. Run shell commands to build the frontend Angular app
- cd app 
- npm i
- ng build --prod
- cd ..

## 5. Adjust Operating System configuration
- add a line "127.0.0.1	town.dev.r1rationality.com" to your platform's hosts file:
  - If you're using Unix or MacOS: /etc/hosts
  - If you're using Windows: C:\Windows\System32\etc\drivers\hosts
  - This step is necessary to make https work - since all certificates are domain bound.

## Explore your local set up
- open https://town.dev.r1rationality.com/
  - all requests are being servшced by your local copy.
  - we have provided you with sample SSL certificates for *.dev.r1rationality.com domains.
    - N.B: Please, watch for their expiration dates and when necessary pull the most recent repo.

# Data schema of the example/data.json and expected by import.js

## index
The name of the Elastic Search index that will be created and updates.
Should be matching the name of the domain of the town.

## schema
Elastic Search type mappings. You can read more details here:
https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping.html

## docs
This section contains individual pages to be presented to the users.

| field              |description                                                                                                |
|--------------------|-----------------------------------------------------------------------------------------------------------|
| title              | The title of the page.                                                                                    |
| description        | The title of the page for the preview cards.                                                              |
| content            | The text to analyze while creating a search index for this document.                                      |
| category           | The category of the page. It'll be shown on preview cards and used for navigational purposes.             |
| safeHtml           | An HTML representation of the document. This will be used to render the content page.                     |
| type               | The name of the preview card template that works the best for this page.                                  |
| typeParams         | Additional data for rendering of specific types of preview cards.                                         |
| url                | The origial URL of the page.                                                                              |
| urlNew             | The URL that will be used by the town portal software.                                                    |
| hash - optional    | The hash of the page contents.                                                                            |
| created - optional | The date when the original page was created.                                                              |
| updated - options  | The date when the original page was last updated.                                                         |

## images
This sections contains the list of images along with their remote and local addresses.
To provide secure https experience, all images need to be served over https as well.
So, to achieve that, we download all images locally in the import.js script.
